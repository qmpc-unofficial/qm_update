FROM rust:1.78-bookworm as builder

WORKDIR /app

# Cache dependencies
COPY Cargo.* .
RUN mkdir src && echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs
RUN cargo build --release && rm -rf src

# Build source
COPY src src
RUN touch src/main.rs && cargo build --release

FROM debian:bookworm-20210816-slim

WORKDIR /app
COPY --from=builder /app/target/release/qm_update ./qm_update
CMD ./qm_update
