use serde_derive::Deserialize;
use serde_derive::Serialize;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateCheckResponse {
    pub code: i64,
    pub ts: i64,
    #[serde(rename = "start_ts")]
    pub start_ts: i64,
    pub traceid: String,
    #[serde(rename = "platform.uniteUpdate.UniteUpdateSvr.QueryUpdate")]
    pub update_response: UniteUpdateServiceResponse,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UniteUpdateServiceResponse {
    pub code: i64,
    pub data: UniteUpdateServiceResponseData,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UniteUpdateServiceResponseData {
    pub pkg_id: i64,
    pub pkg_ct: i64,
    pub pkg_name: String,
    pub pkg_title: String,
    pub pkg_version: i64,
    pub pkg_patch: i64,
    pub pkg_desc: String,
    pub pkg_url: String,
    pub pkg_size: i64,
    pub pkg_hash: String,
    pub min_sys_ver: i64,
    pub ver_type: i64,
    pub use_free_cdn: i64,
    pub cdn_free_url: String,
    pub channel_type: String,
    pub up_interval: i64,
    pub upgrade_type: i64,
    pub pkg_download_type: i64,
    pub other_data: String,
}
