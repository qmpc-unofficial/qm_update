use crate::updater::UpdaterError;
use crate::utils::{parse_version_string, UpdateInfo, UpdateInfoGeneric};
use reqwest::Client;

mod payload;
mod types;

pub async fn fetch_update() -> Result<UpdateInfo, UpdaterError> {
    let payload = payload::generate();

    let client = Client::new();
    let res = client
        .post("https://u6.y.qq.com/cgi-bin/musicu.fcg")
        .header("Content-Type", "application/json")
        .header(
            "User-Agent",
            "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)",
        )
        .body(payload)
        .send()
        .await?;

    let body = res.json::<types::UpdateCheckResponse>().await?;
    let data = &body.update_response.data;

    let version = format!("{}{}", data.pkg_version, data.pkg_patch);
    let (version, major) = parse_version_string(version)?;
    
    Ok(UpdateInfo::Win32(UpdateInfoGeneric {
        version,
        version_major: major,
        description: data.pkg_desc.to_string().replace("\\n", "\n"),
        url: data.pkg_url.replace("http://", "https://"),
        checksum: data.pkg_hash.to_string(),
    }))
}
