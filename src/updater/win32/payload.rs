use crate::utils::rnd::{generate_digits, generate_hex_chars};

pub fn generate() -> String {
    let payload = include_str!("payload.json");
    let guid = generate_hex_chars(32);
    let wid = generate_digits(19);

    payload
        .replace("%GUID%", guid.as_str())
        .replace("%WID%", wid.as_str())
}
