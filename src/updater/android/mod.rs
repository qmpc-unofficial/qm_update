use crate::updater::utils::get_unite_update;
use crate::updater::UpdaterError;
use crate::utils::config::ConfigRoot;
use crate::utils::{enforce_https_url, parse_version_string, UpdateInfo, UpdateInfoAndroid};

pub async fn fetch_update(config: &ConfigRoot) -> Result<UpdateInfo, UpdaterError> {
    let update = get_unite_update(11, config).await?;

    let (version, major) = parse_version_string(update.version)?;

    Ok(UpdateInfo::Android(UpdateInfoAndroid {
        version,
        version_major: major,
        description: update.description,
        url: enforce_https_url(update.url),
        checksum: update.md5,
        url_arm64: enforce_https_url(update.url_arm64),
        checksum_arm64: update.md5_arm64,
    }))
}
