use kiss_xml::errors::KissXmlError;

pub mod android;
pub mod utils;
pub mod win32;
pub mod mac;

#[derive(Debug)]
pub enum UpdaterError {
    NetworkError(reqwest::Error),
    MissingQmaUpdateCheckTool,
    QmaUpdateProcessError(std::io::Error),
    QmaUpdateProcessFailed(String),
    XmlParsingError(KissXmlError),
    InvalidVersionString(String),
}

impl From<reqwest::Error> for UpdaterError {
    fn from(value: reqwest::Error) -> Self {
        Self::NetworkError(value)
    }
}

impl From<KissXmlError> for UpdaterError {
    fn from(value: KissXmlError) -> Self {
        Self::XmlParsingError(value)
    }
}
