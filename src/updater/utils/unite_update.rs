use crate::updater::UpdaterError;
use crate::utils::config::ConfigRoot;
use base64::engine::general_purpose::STANDARD as Base64;
use base64::Engine as _;
use kiss_xml::dom::{Element, Node};
use std::process::Stdio;
use tokio::process::Command;

#[derive(Debug)]
pub struct UniteUpdateResult {
    pub version: String,
    pub title: String,
    pub description: String,

    pub url: String,
    pub md5: String,

    pub url_arm64: String,
    pub md5_arm64: String,
}

trait XmlHelper {
    fn get_tag_value(&self, tag_name: &str) -> String;
    fn get_tag_value_b64(&self, tag_name: &str) -> String;
}

impl XmlHelper for Element {
    fn get_tag_value(&self, tag_name: &str) -> String {
        match self.search_elements_by_name(tag_name).next() {
            Some(tag) => tag.text(),
            None => String::default(),
        }
    }

    fn get_tag_value_b64(&self, tag_name: &str) -> String {
        let value = self.get_tag_value(tag_name);
        let value = Base64.decode(value).unwrap_or_default();
        String::from_utf8_lossy(&value[..]).to_string()
    }
}

pub async fn get_unite_update(
    code: i64,
    config: &ConfigRoot,
) -> Result<UniteUpdateResult, UpdaterError> {
    let qm_update = &config.tools.qma_update_check;
    if qm_update.is_empty() {
        Err(UpdaterError::MissingQmaUpdateCheckTool)?;
    }
    let mut cmd = Command::new(qm_update);

    cmd.stdout(Stdio::null());
    cmd.stderr(Stdio::null());
    cmd.arg(format!("-c={}", code));

    let mut child = cmd.spawn().map_err(UpdaterError::QmaUpdateProcessError)?;
    let output = cmd
        .output()
        .await
        .map_err(UpdaterError::QmaUpdateProcessError)?;
    let exit_status = child
        .wait()
        .await
        .map_err(UpdaterError::QmaUpdateProcessError)?;
    if !exit_status.success() {
        let stderr = String::from_utf8_lossy(&output.stderr[..]).to_string();
        Err(UpdaterError::QmaUpdateProcessFailed(stderr))?;
    }

    let stdout = String::from_utf8_lossy(&output.stdout[..]).to_string();
    // eprintln!("stdout: {}", stdout);

    let dom = kiss_xml::parse_str(stdout)?;
    let root = dom.root_element();

    let version = root.get_tag_value("version");
    if version.len() < 5 {
        return Err(UpdaterError::InvalidVersionString(version));
    }
    
    Ok(UniteUpdateResult {
        version,
        title: root.get_tag_value_b64("title"),
        description: root.get_tag_value_b64("desc"),
        url: root.get_tag_value("url"),
        md5: root.get_tag_value("md5"),
        url_arm64: root.get_tag_value("url64"),
        md5_arm64: root.get_tag_value("md564"),
    })
}
