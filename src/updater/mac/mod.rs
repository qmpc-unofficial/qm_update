use crate::updater::utils::get_unite_update;
use crate::updater::UpdaterError;
use crate::utils::config::ConfigRoot;
use crate::utils::{fix_mac_url, parse_version_string, UpdateInfo, UpdateInfoGeneric};

pub async fn fetch_update(config: &ConfigRoot) -> Result<UpdateInfo, UpdaterError> {
    let update = get_unite_update(6, config).await?;

    let (version, major) = parse_version_string(update.version)?;

    Ok(UpdateInfo::Mac(UpdateInfoGeneric {
        version,
        version_major: major,
        description: update.description,
        url: fix_mac_url(update.url),
        checksum: String::from("N/A"),
    }))
}
