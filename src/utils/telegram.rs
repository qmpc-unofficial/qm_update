use crate::utils::update_types::UpdateMessageDisplay;
use std::fmt::{Display, Formatter};
use teloxide::prelude::*;
use teloxide::types::{ParseMode, Recipient};
use teloxide::{Bot, RequestError};

#[derive(Debug)]
pub enum TelegramPostError {
    InvalidRecipient(String),
    TelegramBotError(RequestError),
}

impl Display for TelegramPostError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TelegramPostError::InvalidRecipient(target) => {
                if target.is_empty() {
                    write!(f, "telegram recipient was empty.",)
                } else {
                    write!(f, "invalid telegram recipient: {}", target)
                }
            }
            TelegramPostError::TelegramBotError(err) => {
                write!(f, "tg error: {}", err)
            }
        }
    }
}

pub async fn post_update<M>(
    chat_target: String,
    bot_token: String,
    update_info: &M,
) -> Result<(), TelegramPostError>
where
    M: UpdateMessageDisplay,
{
    // e.g. "@example_ch"
    let recipient = if chat_target.starts_with('@') {
        Recipient::ChannelUsername(chat_target)
    } else if let Ok(id) = chat_target.parse::<i64>() {
        Recipient::Id(ChatId(id))
    } else {
        return Err(TelegramPostError::InvalidRecipient(chat_target));
    };

    let bot = Bot::new(bot_token).parse_mode(ParseMode::MarkdownV2);
    bot.send_message(recipient, update_info.to_markdown_v2_message())
        .await
        .map_err(TelegramPostError::TelegramBotError)?;

    Ok(())
}
