use serde_derive::Deserialize;
use serde_derive::Serialize;
use std::fmt::{Display, Formatter};
use tokio::fs::File;
use tokio::io::AsyncReadExt;

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ConfigRoot {
    pub tools: Tools,
    pub telegram: ConfigTelegramBot,
    #[serde(rename = "cache_dir")]
    pub cache_directory: CacheDirectory,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Tools {
    #[serde(rename = "qma_update_check", default)]
    pub qma_update_check: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CacheDirectory {
    #[serde(default = "default_cache_win32")]
    pub win32: String,
    #[serde(default = "default_cache_android")]
    pub android: String,
    #[serde(default = "default_cache_mac")]
    pub mac: String,
}

fn default_cache_win32() -> String {
    String::from("versions/win32/")
}
fn default_cache_android() -> String {
    String::from("versions/android/")
}
fn default_cache_mac() -> String {
    String::from("versions/mac/")
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase", default)]
pub struct ConfigTelegramBot {
    #[serde(default)]
    pub token: String,
    #[serde(default)]
    pub target: String,
}

#[derive(Debug)]
pub enum ConfigParsingError {
    IOError(std::io::Error),
    TomlParsingError(toml::de::Error),
}

impl Display for ConfigParsingError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ConfigParsingError::IOError(err) => err.fmt(f),
            ConfigParsingError::TomlParsingError(err) => err.fmt(f),
        }
    }
}

pub async fn from_file<T: AsRef<str>>(file_path: T) -> Result<ConfigRoot, ConfigParsingError> {
    let mut file = File::open(file_path.as_ref())
        .await
        .map_err(ConfigParsingError::IOError)?;
    let mut toml_body = String::new();
    file.read_to_string(&mut toml_body)
        .await
        .map_err(ConfigParsingError::IOError)?;
    toml::from_str(toml_body.as_str()).map_err(ConfigParsingError::TomlParsingError)
}
