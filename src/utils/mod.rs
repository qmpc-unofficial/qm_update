pub mod config;
pub mod rnd;
pub mod telegram;
mod update_types;
mod str_manip;
mod version_parser;

pub use update_types::{UpdateInfo, UpdateInfoAndroid, UpdateInfoGeneric, UpdateMessageDisplay};
pub use version_parser::parse_version_string;

pub fn enforce_https_url<T: AsRef<str>>(url: T) -> String {
    str_manip::replace_prefix(url, "http://", "https://")
}

pub fn fix_mac_url<T: AsRef<str>>(url: T) -> String {
    let url = enforce_https_url(url);
    str_manip::replace_suffix(url, ".zip", ".dmg")
}
