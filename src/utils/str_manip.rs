pub fn replace_suffix<T: AsRef<str>>(input: T, suffix: &str, replacement: &str) -> String {
    let input = input.as_ref();
    match input.strip_suffix(suffix) {
        Some(prefix) => format!("{}{}", prefix, replacement),
        None => String::from(input)
    }
}

pub fn replace_prefix<T: AsRef<str>>(input: T, prefix: &str, replacement: &str) -> String {
    let input = input.as_ref();
    match input.strip_prefix(prefix) {
        Some(suffix) => format!("{}{}", replacement, suffix),
        None => String::from(input)
    }
}
