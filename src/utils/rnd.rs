use rand::distributions::Alphanumeric;
use rand::Rng;

pub fn generate_hex_chars(n: usize) -> String {
    let random_bytes = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(n)
        .collect::<Vec<u8>>();
    hex::encode(random_bytes)
}

pub fn generate_digits(n: usize) -> String {
    let mut rng = rand::thread_rng();
    std::iter::repeat(())
        .map(|_| rng.sample(Alphanumeric))
        .filter(|c: &u8| char::is_numeric(char::from(*c)))
        .take(n)
        .map(|c: u8| char::from(c))
        .collect()
}
