use lazy_static::lazy_static;
use regex::Regex;
use crate::updater::UpdaterError;

lazy_static! {
    static ref RE_VERSION_MATCHER_3_PARTS: Regex = Regex::new(r"^(\d+)(\d{2})(\d{2})$").unwrap();
    static ref RE_VERSION_MATCHER_4_PARTS: Regex = Regex::new(r"^(\d+)(\d{2})(\d{2})(\d{2})$").unwrap();
}

pub fn parse_version_string<T: AsRef<str>>(version: T) -> Result<(String, String), UpdaterError> {
    let version = version.as_ref();

    // Format version string, "XXYYZZAA" or "XXYYZZ"
    let re_version_matcher = if version.len() > 6 {
        RE_VERSION_MATCHER_4_PARTS.clone()
    } else {
        RE_VERSION_MATCHER_3_PARTS.clone()
    };

    match re_version_matcher.captures(version) {
        None => Err(UpdaterError::InvalidVersionString(String::from(version)))?,
        Some(caps) => {
            let major = &caps[1].parse::<u64>().unwrap_or_default();
            let minor = &caps[2].parse::<u64>().unwrap_or_default();
            let patch = &caps[3].parse::<u64>().unwrap_or_default();
            let full_version = format!("{}.{}.{}", major, minor, patch);
            let main_version_id = format!("{}{:0>2}", major, minor);
            Ok((full_version, main_version_id))
        }
    }
}
