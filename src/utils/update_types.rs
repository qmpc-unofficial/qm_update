use indoc::formatdoc;
use serde_derive::{Deserialize, Serialize};

pub trait UpdateMessageDisplay {
    fn to_markdown_v2_message(&self) -> String;
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateInfoGeneric {
    pub version: String,
    pub version_major: String,
    pub description: String,
    pub url: String,
    pub checksum: String,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct UpdateInfoAndroid {
    pub version: String,
    pub version_major: String,
    pub description: String,
    pub url: String,
    pub checksum: String,
    pub url_arm64: String,
    pub checksum_arm64: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum UpdateInfo {
    Win32(UpdateInfoGeneric),
    Android(UpdateInfoAndroid),
    Mac(UpdateInfoGeneric),
}

impl UpdateInfo {
    pub fn get_version(&self) -> String {
        match self {
            UpdateInfo::Win32(v) => v.version.clone(),
            UpdateInfo::Android(v) => v.version.clone(),
            UpdateInfo::Mac(v) => v.version.clone(),
        }
    }

    pub fn get_json(&self) -> String {
        match self {
            UpdateInfo::Win32(v) => serde_json::to_string(&v).unwrap(),
            UpdateInfo::Android(v) => serde_json::to_string(&v).unwrap(),
            UpdateInfo::Mac(v) => serde_json::to_string(&v).unwrap(),
        }
    }
}

impl UpdateMessageDisplay for UpdateInfo {
    fn to_markdown_v2_message(&self) -> String {
        match self {
            UpdateInfo::Win32(info) => {
                formatdoc! {"
                    \\#Windows \\#QQ音乐 发现新版本：v{version}
                    \\#v{version_major}

                    更新日志：
                    ```
                    {desc}
                    ```
                    [下载安装程序]({url}) \\| [`archive.org` 存档](https://web.archive.org/web/*/{url})
                    校验 `{checksum}`
                ",
                    version_major = info.version_major,
                    version = info.version.replace('.', "\\."),
                    desc = info.description,
                    url = info.url,
                    checksum = info.checksum,
                }
            }
            UpdateInfo::Android(info) => {
                formatdoc! {"
                    \\#安卓 \\#QQ音乐 发现新版本：v{version}
                    \\#v{version_major}

                    更新日志：
                    ```
                    {desc}
                    ```
                    \\- [下载 \\(arm\\)]({url}) \\| [`archive.org` 存档](https://web.archive.org/web/*/{url})
                      \\- 校验 `{checksum}`
                    \\- [下载 \\(arm64\\)]({url_arm64}) \\| [`archive.org` 存档](https://web.archive.org/web/*/{url_arm64})
                      \\- 校验 `{checksum_arm64}`
                ",
                    version_major = info.version_major,
                    version = info.version.replace('.', "\\."),
                    desc = info.description,
                    url = info.url,
                    checksum = info.checksum,
                    url_arm64 = info.url_arm64,
                    checksum_arm64 = info.checksum_arm64,
                }
            },
            UpdateInfo::Mac(info) => {
                formatdoc! {"
                    \\#Mac \\#QQ音乐 发现新版本：v{version}
                    \\#v{version_major}

                    更新日志：
                    ```
                    {desc}
                    ```
                    [下载]({url}) \\| [`archive.org` 存档](https://web.archive.org/web/*/{url})
                ",
                    version_major = info.version_major,
                    version = info.version.replace('.', "\\."),
                    desc = info.description,
                    url = info.url,
                }
            }
        }
    }
}
