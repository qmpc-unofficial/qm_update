mod updater;
mod utils;

use std::io::ErrorKind;
use crate::updater::UpdaterError;
use crate::utils::{telegram, UpdateMessageDisplay};
use clap::Parser;
use serde_derive::Serialize;
use std::process::exit;
use tokio::fs::OpenOptions;
use tokio::io::AsyncWriteExt;

#[derive(clap::ValueEnum, Clone, Default, Debug, Serialize)]
#[serde(rename_all = "kebab-case")]
enum UpdateSource {
    // Windows update check.
    #[default]
    Win32,
    // Check for Mac updates.
    // Requires `qma-update-check`.
    Mac,
    // Check for Android updates.
    // Requires `qma-update-check`.
    Android,
}

/// A simple update fetch and notify program.
#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Which platform to check update for?
    #[arg(short, long)]
    source: UpdateSource,

    /// Path to config file
    #[arg(short, long, default_value_t = String::from("config.toml"))]
    config: String,
}

//noinspection HttpUrlsUsage
#[tokio::main]
async fn main() -> Result<(), UpdaterError> {
    let args = Args::parse();

    let config = match utils::config::from_file(args.config).await {
        Ok(c) => c,
        Err(err) => {
            eprintln!("Error while loading config: {}", err);
            exit(1);
        }
    };

    let update = match args.source {
        UpdateSource::Win32 => updater::win32::fetch_update().await,
        UpdateSource::Android => updater::android::fetch_update(&config).await,
        UpdateSource::Mac => updater::mac::fetch_update(&config).await,
    }?;

    let update_cache_dir = match args.source {
        UpdateSource::Win32 => &config.cache_directory.win32,
        UpdateSource::Mac => &config.cache_directory.mac,
        UpdateSource::Android => &config.cache_directory.android,
    };

    eprintln!("Recording version {}...", update.get_version());
    if let Err(err) = std::fs::create_dir_all(update_cache_dir) {
        eprintln!("could not create cache dir `{}`: {}", update_cache_dir, err);
        exit(1);
    }

    let update_json_path = format!("{}/{}.json", update_cache_dir, update.get_version());
    eprintln!("caching to `{}`...", update_json_path);
    match OpenOptions::new()
        .write(true)
        .create_new(true)
        .open(update_json_path)
        .await
    {
        Ok(mut f) => f.write_all(update.get_json().as_bytes()).await,
        Err(err) if err.kind() == ErrorKind::AlreadyExists => {
            eprintln!("Skip: {}", err);
            exit(0);
        },
        Err(err) => {
            eprintln!("Failed to write cache: {}", err);
            exit(1);
        },
    }.expect("Filesystem I/O Error!");

    eprintln!("message: {}", update.to_markdown_v2_message());

    eprintln!("Update to Telegram...");
    match telegram::post_update(config.telegram.target, config.telegram.token, &update).await {
        Ok(_) => {
            eprintln!("ok!");
        }
        Err(err) => {
            eprintln!("failed to post to Telegram: {}", err);
        }
    }

    Ok(())
}
