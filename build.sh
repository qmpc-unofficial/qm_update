#!/bin/sh

mkdir -p dist
docker build --tag qm_update .
docker run --rm -it -v "$PWD":/workdir qm_update \
  install -o $(id -u) -g $(id -g) \
    qm_update /workdir/dist/qm_update
chmod a+x dist/qm_update
